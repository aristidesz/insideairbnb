--set schema
set search_path to public;

--function
create or replace function count_rows(tablename text) returns void as
$func$
begin

execute

$$

DROP TABLE IF EXISTS rows_of_table_$$ || tablename || $$;
CREATE TABLE rows_of_table_$$ || tablename || $$ as
select count(*) FROM $$ || tablename || $$;


$$;
end

$func$
language plpgsql;

SELECT count_rows('airbnb_London')
