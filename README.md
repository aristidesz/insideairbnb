
# Airbnb London Project

This repository uses the listings of Airbnb gathered from inside Airbnb at 10/07/2019. You can view what has been done in "DB Set-up.html" and "Regression.html".

## Regression

Linear regression, adaboost, decision tree and random forest regression were performed on the dataset to obtain the price of listings in London. The most valuable predictive feature was identified to be the location of the accomodation. Significant work can be done on this (see future work).

### Prerequisites

We have used pandas dataframes, jupyter notebooks, python, sklearn and matplotlib.

Initially we perform a data analysis, feature engineering and then we perform the modeling (train/test).

## Import to DB

Ideally, we would like to import the data to a database but since we have the listings for only one particular date this is not necessary. However, the method for importing the data in the db is outlined in "DB Set-up.ipynb" notebook.

### Postgre, pgadmin and SQLAlchemy

This was achieved with three tools "postgre", "pgadmin" and "SQLAlchemy"

## Future work

* Encode blank fields to indicate that column corrupt.
* Perform a histogram on features to identify outliers.
* Link zipcode with London zones (zone 1/2 etc).
* Connect with Google API and associate GPS location distance from city centre or distance from the train station or underground/overground station.
* Use NLP with sentiment analysis to understand whether reviews are positive or negative (find overall feeling).
* Use image classification (object recognition) to relate expensive or inexpensive property.
* Topic modelling to discover the topic associated with each listing and group by topic.
* Perform an ensemble learning (stacking) to improve predictions.

## Built With

* [Scikit-learn: Machine Learning in Python](https://scikit-learn.org/stable/) - Scikit-learn: Machine Learning in Python, Pedregosa et al., JMLR 12, pp. 2825-2830, 2011.

## Authors

* **Aristides Zenonos** -  [AristidesZenonos](https://www.aristideszenonos.com/)



